
-- Copyright (C) 2017-2019 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


local _, messaging = DLib.MessageMaker({}, 'DSit')
DLib.chat.generate('DSit', messaging)

net.pool('DSit.VehicleTick')
net.pool('dsit_confirm_sit')

local DSIT_TRACKED_VEHICLES = _G.DSIT_TRACKED_VEHICLES
local TRANSLUCENT = Color(255, 255, 255, 0)

local function makeVehicle(owner, pos, ang)
	local ent = ents.Create('prop_vehicle_prisoner_pod')
	ent:SetModel('models/nova/airboat_seat.mdl')
	ent:SetKeyValue('vehiclescript', 'scripts/vehicles/prisoner_pod.txt')
	ent:SetKeyValue('limitview', '0')
	ent:SetPos(pos)
	ent:SetAngles(ang)
	ent:Spawn()
	ent:Activate()

	ent:SetMoveType(MOVETYPE_NONE)
	ent:SetCollisionGroup(COLLISION_GROUP_WORLD)
	ent:SetNWBool('dsit_flag', true)
	ent:SetNotSolid(true)
	ent:DrawShadow(false)
	ent:SetColor(TRANSLUCENT)
	ent:SetRenderMode(RENDERMODE_TRANSALPHA)
	ent:SetNoDraw(true)

	if owner and ent.CPPISetOwner then
		ent:CPPISetOwner(owner)
	end

	local phys = ent:GetPhysicsObject()

	if IsValid(phys) then
		phys:Sleep()
		phys:EnableGravity(false)
		phys:EnableMotion(false)
		phys:EnableCollisions(false)
		phys:SetMass(1)
	end

	return ent
end

local function inRange(x, min, max)
	return x > min and x < max
end

local function checkNormal(ply, normal)
	local normalCheck = normal:Angle()
	normalCheck.p = normalCheck.p - 270

	if not inRange(normalCheck.p, -20, 20) and (not DSitConVars:getBool('allow_ceiling') or not inRange(normalCheck.p, -190, -170) and not inRange(normalCheck.p, 170, 190)) then
		messaging.LChatPlayer2(ply, 'message.dsit.check.pitch', normalCheck.p)
		return false
	end

	if normalCheck.r > 20 or normalCheck.r < -20 then
		messaging.LChatPlayer2(ply, 'message.dsit.check.roll', normalCheck.r)
		return false
	end

	return true
end

-- this is stupid
local function PlayerPostThink(ply)
	if not ply.__dsit_request then return end
	local args = ply.__dsit_request
	ply.__dsit_request = nil
	local maxVelocity = DSitConVars:getFloat('speed_val')

	if not ply:Alive() then return end
	-- if ply.dsit_pickup then return end

	if ply:InVehicle() then return end

	local mins, maxs = ply:GetHull()
	local eyes = ply:EyePos()
	local spos = ply:GetPos()
	local ppos = spos + ply:OBBCenter()
	local fwd = ply:GetAimVector()
	maxs.z = 0
	mins.z = 0

	local preferTargetPos = false

	for i, arg in ipairs(args) do
		if arg:startsWith('pos:') then
			preferTargetPos = arg:sub(5)
		end
	end

	if preferTargetPos then
		local split = preferTargetPos:split(',')

		if #split == 3 then
			local x, y, z = tonumber(split[1]), tonumber(split[2]), tonumber(split[3])

			if x and y and z then
				fwd = (Vector(x, y, z) - eyes):GetNormalized()
			end
		end
	end

	local trDataLine = {
		start = eyes,
		endpos = eyes + fwd * DSitConVars:getFloat('distance'),
		filter = ply
	}

	local trDataHull = {
		start = ppos,
		endpos = trDataLine.endpos,
		filter = ply,
		mins = mins,
		maxs = maxs,
	}

	ply:LagCompensation(true)
	local tr = util.TraceLine(trDataLine)
	local trh = util.TraceHull(trDataHull)
	ply:LagCompensation(false)

	if not tr.Hit then
		return
	end

	if IsValid(tr.Entity) and tr.Entity:GetClass():startsWith('func_door') then
		return
	end

	--[[if not (type(trh.Entity) == 'Player' and not IsValid(tr.Entity)) then
		if type(tr.Entity) ~= 'Player' and tr.Entity ~= trh.Entity then
			messaging.LChatPlayer2(ply, 'message.dsit.check.unreachable')
			return
		end
	end]]

	if type(trh.Entity) ~= 'Player' and IsValid(trh.Entity) and not IsValid(tr.Entity) then
		messaging.LChatPlayer2(ply, 'message.dsit.check.unreachable')
		return
	end

	local isPlayer, isEntity, isNPC, isSitting, entSit, parent = false, false, false, false, NULL, false
	local ent = tr.Entity
	local preferForward, preferBackward, preferSnap, noSnap = table.qhasValue(args, 'prefer-forward'), table.qhasValue(args, 'prefer-backward'), table.qhasValue(args, 'prefer-snap'), table.qhasValue(args, 'no-snap')
	local noPreferDirection = preferForward == false and preferBackward == false
	local shouldSnap = preferSnap or not noSnap

	local preferAngle = false

	for i, arg in ipairs(args) do
		if arg:startsWith('angle:') then
			preferAngle = tonumber(arg:sub(7)) or false
		end
	end

	if preferAngle then
		preferAngle = preferAngle:normalizeAngle()
	end

	if IsValid(ent) then
		if not DSitConVars:getBool('entities') then
			messaging.LChatPlayer2(ply, 'message.dsit.status.entities')
			return
		end

		if type(ent) == 'NPC' or type(ent) == 'NextBot' then
			if not DSitConVars:getBool('npcs') then
				messaging.LChatPlayer2(ply, 'message.dsit.status.npc')
				return
			end

			isNPC = true
		end

		if maxVelocity > 0 and ply:GetMoveType() ~= MOVETYPE_NOCLIP then
			if ent:GetVelocity():Length() >= maxVelocity then
				messaging.LChatPlayer2(ply, 'message.dsit.status.toofast')
				return
			end
		end

		isPlayer = type(ent) == 'Player'
		isEntity = true

		if isPlayer then
			entSit = ent:GetVehicle()

			if IsValid(entSit) then
				isSitting = entSit:GetNWBool('dsit_flag')
			end

			parent = false
		--elseif isNPC then
		--  parent = false
		else
			isSitting = ent:GetNWBool('dsit_flag')
			entSit = ent
			parent = not isSitting
		end

		if ent.dsit_player_root == ply and ent:InVehicle() then
			messaging.LChatPlayer2(ply, 'message.dsit.status.recursion')
			return
		end

		if ent.dsit_player_root == ply and not ent:InVehicle() then
			ent.dsit_player_root = nil
		end
	end

	local targetPos, targetAngles
	local upsideDown = false

	if isSitting then
		if not DSitConVars:getBool('players_legs') then
			messaging.LChatPlayer2(ply, 'message.dsit.status.nolegs')
			return
		end

		targetAngles = entSit:GetAngles()
		local fwdAngles = entSit:GetAngles()
		fwdAngles.p = 0
		fwdAngles.r = 0
		fwdAngles.y = fwdAngles.y + 90
		targetPos = entSit:GetPos() + fwdAngles:Forward() * 16 + targetAngles:Up() * 8
	elseif isPlayer then
		if not DSitConVars:getBool('players') then
			messaging.LChatPlayer2(ply, 'message.dsit.status.noplayers')
			return
		end

		if not ent:GetInfoBool('cl_dsit_allow_on_me', true) then
			messaging.LChatPlayer2(ply, 'message.dsit.status.diasallowed')
			return
		end

		if ent:GetInfoBool('cl_dsit_ask', false) and not (ent.__dsit_confirmed and ent.__dsit_confirmed[ply] and ent.__dsit_confirmed[ply] > RealTime() or ent:GetInfoBool('cl_dsit_ask_auto', true) and ent:CheckDLibFriendIn2(ply, 'dsit')) then
			ply.__dsit_confirm_spam = ply.__dsit_confirm_spam or {}

			if (not ply.__dsit_confirm_spam[ent] or ply.__dsit_confirm_spam[ent] < RealTime()) then
				messaging.LChatPlayer2(ply, 'message.dsit.status.confirmation_sent', ent)
				ply.__dsit_confirm_spam[ent] = RealTime() + 5
				ent.__dsit_confirm_last = ply

				net.Start('dsit_confirm_sit')
				net.WritePlayer(ply)
				net.Send(ent)
			end

			return
		end

		do
			local target = ent:GetInfoBool('cl_dsit_friendsonly', false)
			local me = ply:GetInfoBool('cl_dsit_friendsonly', false)
			local isFriends = ply:CheckDLibFriendIn2(ent, 'dsit') and ent:CheckDLibFriendIn2(ply, 'dsit')

			if (target or me) and not isFriends then
				messaging.LChatPlayer2(ply, 'message.dsit.status.friendsonly')
				return
			end
		end

		targetAngles = ent:EyeAngles()
		targetPos = ent:EyePos()

		targetAngles.p = 0
		targetAngles.r = 0
	elseif isNPC then
		targetAngles = ent:EyeAngles()
		targetAngles.y = targetAngles.y - 90
		targetPos = ent:EyePos()

		if type(ent) == 'NextBot' then
			targetPos = ent:GetPos()
			targetPos.z = targetPos.z + ent:OBBMaxs().z
		end

		targetAngles.p = 0
		targetAngles.r = 0
	elseif isEntity then
		if DSitConVars:getBool('entities_world') and ent.CPPIGetOwner and IsValid(ent:CPPIGetOwner()) then
			messaging.LChatPlayer2(ply, 'message.dsit.status.nonowned')
			return
		end

		if DSitConVars:getBool('entities_owner') and (ent.CPPIGetOwner and ent:CPPIGetOwner() or NULL) ~= ply then
			messaging.LChatPlayer2(ply, 'message.dsit.status.onlyowned')
			return
		end

		if not DSitConVars:getBool('anyangle') and not checkNormal(ply, tr.HitNormal) then
			return
		end

		if preferAngle then
			targetAngles = Angle(0, preferAngle, 0)
		else
			targetAngles = ply:EyeAngles()

			if not preferForward and (tr.HitPos:Distance(ply:GetPos()) < 30 or preferBackward) then
				targetAngles.y = targetAngles.y + 90
			else
				targetAngles.y = targetAngles.y - 90
			end

			targetAngles.r = 0
			targetAngles.p = 0

			if shouldSnap then
				local checkAngle = ply:EyeAngles()

				if not preferForward and not preferBackward then
					-- let's guess, probably forward
					local checkAngle = ply:EyeAngles()
					checkAngle.p = 0
					checkAngle.y = checkAngle.y - 180
					checkAngle.r = 0

					foundSnap, snapNormal = DSit_FindSnappyAngle(ply, tr, checkAngle)

					if not foundSnap then
						checkAngle.y = checkAngle.y + 180
						foundSnap, snapNormal = DSit_FindSnappyAngle(ply, tr, checkAngle)
					end
				else
					foundSnap, snapNormal = DSit_FindSnappyAngle(ply, tr, targetAngles)
				end

				if foundSnap then
					targetAngles = snapNormal:Angle()
					targetAngles.r = 0
					targetAngles.y = targetAngles.y - 90
					targetAngles.p = 0
				end
			end
		end

		targetPos = tr.HitPos - tr.HitNormal * 4
	else
		if not DSitConVars:getBool('anyangle') and not checkNormal(ply, tr.HitNormal) then
			return
		end

		local normalAngle = tr.HitNormal:Angle()
		normalAngle.p = normalAngle.p - 270

		targetPos = tr.HitPos - tr.HitNormal * 2

		if preferAngle then
			targetAngles = Angle(0, preferAngle, 0)
		else
			local foundSnap, snapNormal

			if shouldSnap then
				local checkAngle = ply:EyeAngles()

				if not preferForward and not preferBackward then
					-- let's guess, probably forward
					local checkAngle = ply:EyeAngles()
					checkAngle.p = 0
					checkAngle.y = checkAngle.y - 180
					checkAngle.r = 0

					foundSnap, snapNormal = DSit_FindSnappyAngle(ply, tr, checkAngle)

					if not foundSnap then
						checkAngle.y = checkAngle.y + 180
						foundSnap, snapNormal = DSit_FindSnappyAngle(ply, tr, checkAngle)
					end
				else
					foundSnap, snapNormal = DSit_FindSnappyAngle(ply, tr, targetAngles)
				end
			end

			if foundSnap then
				targetAngles = snapNormal:Angle()
				targetAngles.r = 0
				targetAngles.y = targetAngles.y - 180
				targetAngles.p = 0
			elseif not preferForward and (tr.HitPos:Distance(ply:GetPos()) < 30 or preferBackward) then
				targetAngles = ply:EyeAngles()
				targetAngles.y = targetAngles.y
				targetAngles.r = 0
				targetAngles.p = 0
			elseif preferForward then
				targetAngles = ply:EyeAngles()
				targetAngles.y = targetAngles.y - 180
				targetAngles.r = 0
				targetAngles.p = 0
			else
				local fwdang = ply:EyeAngles()
				fwdang.p = 0
				fwdang.r = 0

				local trForward = util.TraceLine({
					start = tr.HitPos + tr.HitNormal * 2,
					endpos = tr.HitPos + tr.HitNormal * 2 + fwdang:Forward() * 40,
					filter = ply
				})

				local unhit = true

				if not trForward.Hit then
					local newTr2 = util.TraceLine({
						start = trForward.HitPos + tr.HitNormal * 10,
						endpos = trForward.HitPos - tr.HitNormal * 10,
						filter = ply
					})

					if not newTr2.Hit or newTr2.Fraction > 0.65 then
						unhit = false
						targetAngles = ply:EyeAngles()
						targetAngles.r = 0
						targetAngles.p = 0
						targetAngles.y = targetAngles.y - 180
					end
				end

				if unhit then
					targetAngles = ply:EyeAngles()
					targetAngles.y = targetAngles.y
					targetAngles.r = 0
					targetAngles.p = 0
				end
			end

			targetAngles.y = targetAngles.y + 90
		end

		if normalAngle.p > 170 or normalAngle.p < -170 then
			targetAngles.y = targetAngles.y - 180
			targetAngles.p = targetAngles.p - 180
			upsideDown = true
		end
	end

	if isPlayer or isSitting then
		local findRoot

		if isPlayer then
			findRoot = ent
		else
			findRoot = ent.dsit_player_root
		end

		findRoot.dsit_root_sitting_on = findRoot.dsit_root_sitting_on or 0
		local max = findRoot:GetInfoInt('cl_dsit_maxonme')

		if max > 0 and max <= findRoot.dsit_root_sitting_on then
			messaging.LChatPlayer2(ply, 'message.dsit.status.restricted', ' (', max, ')')
			return
		end
	end

	local vehicle = makeVehicle(ply, targetPos, targetAngles)
	local can = ply:IsBot() or hook.Run('CanPlayerEnterVehicle', ply, vehicle) ~= false

	if not can then
		messaging.LChatPlayer2(ply, 'message.dsit.status.hook')
		vehicle:Remove()
		return
	end

	local weaponry = DSitConVars:getBool('allow_weapons')
	local flashlight

	if weaponry then
		ply.dsit_weapons = ply:GetAllowWeaponsInVehicle()
		ply:SetAllowWeaponsInVehicle(true)
		flashlight = ply:FlashlightIsOn()
	end

	ply.dsit_old_eyes = ply:EyeAngles()
	ply:DropObject()
	ply:EnterVehicle(vehicle)
	ply:SetEyeAngles(Angle(0, 90, 0))
	ply.dsit_spos = spos

	if weaponry then
		if flashlight then ply:Flashlight(flashlight) end
	end

	ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)

	if parent then
		--[[
		-- won't play nice with other mods since player position is reported to be near vector_origin
		local _pos, _ang = WorldToLocal(vehicle:GetPos(), vehicle:GetAngles(), ent:GetPos(), ent:GetAngles())
		local _dummy = ents.Create('dsit_dummy')
		_dummy:SetPos(_pos)
		_dummy:SetAngles(_ang)
		_dummy:Spawn()
		_dummy:SetMoveParent(ent)
		vehicle:SetPos(Vector())
		vehicle:SetAngles(Angle())
		vehicle:SetParent(_dummy)
		]]

		-- so instead, tell prop protection mods that vehicle is owned by
		-- the player whom prop we sit on top
		if ent.CPPIGetOwner and IsValid(ent:CPPIGetOwner()) then
			vehicle:CPPISetOwner(ent:CPPIGetOwner(), nil)
		end

		ply:LagCompensation(true)
		vehicle:SetParent(ent)
		ply:LagCompensation(false)
	elseif isSitting then
		vehicle.dsit_player_root = ent.dsit_player_root
		vehicle:SetParent(entSit)
	--[==[elseif isNPC then
		vehicle:SetNWEntity('dsit_target', ent)

		--[[timer.Simple(0.5, function()
			net.Start('DSit.VehicleTick')
			net.WriteEntity(vehicle)
			net.Broadcast()
		end)]]

		table.insert(DSIT_TRACKED_VEHICLES, vehicle)]==]
	elseif isPlayer then
		ply.dsit_player_root = ent.dsit_player_root or ent
		vehicle:SetNWEntity('dsit_target', ent)
		vehicle.dsit_player_root = ent

		timer.Simple(0.5, function()
			net.Start('DSit.VehicleTick')
			net.WriteEntity(vehicle)
			net.Broadcast()
		end)

		table.insert(DSIT_TRACKED_VEHICLES, vehicle)
	end

	if vehicle.dsit_player_root then
		vehicle.dsit_player_root.dsit_root_sitting_on = (vehicle.dsit_player_root.dsit_root_sitting_on or 0) + 1
		vehicle:SetNWEntity('dsit_player_root', vehicle.dsit_player_root)
	end

	vehicle.dsit_upsideDown = upsideDown

	vehicle:SetNWEntity('dsit_entity', ply)
	ply:SetNWEntity('dsit_entity', vehicle)
end

local function request(ply, args)
	if not DSitConVars:getBool('enable') then return end
	if not IsValid(ply) then return end
	if not ply:Alive() then return end
	-- if ply.dsit_pickup then return end

	if ply:InVehicle() then return end

	local maxVelocity = DSitConVars:getFloat('speed_val')

	if maxVelocity > 0 and ply:GetMoveType() ~= MOVETYPE_NOCLIP then
		if ply:GetVelocity():Length() >= maxVelocity then
			messaging.LChatPlayer2(ply, 'message.dsit.sit.toofast')
			return
		end
	end

	ply.__dsit_request = args or {}
end

local tonumber = tonumber

local function dsit_getoff(ply, cmd, args)
	if not IsValid(ply) then return end

	local tokick = tonumber((args[1] or ''):trim())
	local pkick = Entity(tokick or -1)

	if pkick:IsValid() then
		for i, vehicle in ipairs(DSIT_TRACKED_VEHICLES) do
			if IsValid(vehicle) then
				if vehicle:GetNWEntity('dsit_player_root', NULL) == ply and vehicle:GetDriver() == pkick then
					vehicle:GetDriver():ExitVehicle()
					break
				end
			end
		end
	else
		for i, vehicle in ipairs(DSIT_TRACKED_VEHICLES) do
			if IsValid(vehicle) then
				local ent = vehicle:GetNWEntity('dsit_target')

				if ent == ply and IsValid(vehicle:GetDriver()) then
					vehicle:GetDriver():ExitVehicle()
				end
			end
		end
	end
end

concommand.Add('dsit', function(a, b, c) request(a, c) end)
concommand.Add('dsit_getoff', dsit_getoff)

_G.DSIT_REQUEST_DEBUG = request

local vectors = {}

for x = -8, 8 do
	for y = -8, 8 do
		table.insert(vectors, Vector(x * 16, y * 16))
	end
end

table.sort(vectors, function(a, b)
	local ax, ay, bx, by, al, bl = a.x:abs(), a.y:abs(), b.x:abs(), b.y:abs(), a:Length(), b:Length()

	if al == bl then
		return ax < bx and ay < by
	end

	return al < bl
end)

local function findPos(ply, pos)
	pos.z = pos.z + 3
	local mins, maxs = ply:OBBMins(), ply:OBBMaxs()
	local realz = math.abs(maxs.z - mins.z)
	local halfz = realz / 2
	local pos2 = Vector(pos.x, pos.y, pos.z + halfz)
	mins.z = 0
	maxs.z = 0

	local trace = {
		filter = ply,
		start = pos2,
		mins, maxs = mins, maxs
	}

	for soften_size = 0, 4 do
		for i, vec in ipairs(vectors) do
			trace.start = pos2
			trace.endpos = pos2 + vec
			trace.endpos.z = trace.endpos.z + halfz

			local tr = util.TraceLine(trace)
			local tr2, tr3
			local hit = tr.Hit

			if not hit then
				local veco = Vector(trace.start)
				trace.start = LerpVector(soften_size / (10 * DSitConVars:getFloat('unstuck_mercy_div')), trace.start, trace.endpos) -- ugh
				tr2 = util.TraceHull(trace)
				trace.start = veco
				hit = tr2.Hit
			else
				DLib.debugoverlay.Cross(tr.HitPos, 5, 5, color_red, true)
			end

			if not hit then
				trace = {
					start = pos + vec,
					endpos = pos + vec,
					filter = ply,
					mins = mins,
					maxs = maxs
				}

				trace.endpos.z = trace.endpos.z + realz

				tr3 = util.TraceLine(trace)
				hit = tr3.Hit

				if not hit then
					tr3 = util.TraceHull(trace)
					hit = tr3.Hit
				end
			end

			DLib.debugoverlay.Line(tr.StartPos, tr.HitPos, 5, not hit and color_green or color_red, true)

			if not hit then
				DLib.debugoverlay.Line(pos2 + vec, pos2 + vec + Vector(0, 0, realz), 5, color_green, true)
				return pos2 + vec
			end
		end
	end
end

local function PostLeave(ply, vehPos, upsideDown)
	if upsideDown then
		local tr = util.TraceLine({
			start = vehPos - Vector(0, 0, 5),
			endpos = vehPos - Vector(0, 0, 400),
			filter = ply
		})

		vehPos = tr.HitPos + tr.HitNormal * 2
	end

	if ply.dsit_weapons ~= nil then
		ply:SetAllowWeaponsInVehicle(ply.dsit_weapons)
		ply.dsit_weapons = nil
	end

	local position = findPos(ply, vehPos)

	if position then
		ply:SetPos(position)
		return
	end

	messaging.LChatPlayer2(ply, 'info.dsit.nopos')
	ply:SetPos(ply.dsit_spos or vehPos)
end

function DSit_PlayerLeaveVehicle(ply, vehicle)
	if not vehicle:GetNWBool('dsit_flag') then return end

	if IsValid(vehicle.dsit_player_root) then
		vehicle.dsit_player_root.dsit_root_sitting_on = math.max(0, (vehicle.dsit_player_root.dsit_root_sitting_on or 1) - 1)
	else
		if ply.dsit_old_eyes then ply:SetEyeAngles(ply.dsit_old_eyes) end
	end

	ply.dsit_player_root = nil
	local upsideDown = vehicle.dsit_upsideDown

	local vehPos = vehicle:GetPos()

	vehicle:Remove()

	timer.Simple(0, function()
		if not IsValid(ply) then return end
		PostLeave(ply, vehPos, upsideDown)
	end)
end

local function PlayerDeath(ply)
	ply.dsit_root_sitting_on = 0
	ply.dsit_player_root = nil
	ply:SetNWBool('dsit_flag', false)
end

local function PlayerSay(ply, text)
	if ply:GetInfoBool('cl_dsit_message', true) and text:lower():find('get off') then
		dsit_getoff(ply, 'dsit_getoff', {})
	end

	if text:trim():startsWith('/dsitc') then
		if IsValid(ply.__dsit_confirm_last) then
			messaging.LChatPlayer2(ply.__dsit_confirm_last, 'message.dsit.status.confirmed', ply)
			messaging.LChatPlayer2(ply, 'message.dsit.status.confirmed2', ply.__dsit_confirm_last)
			ply.__dsit_confirmed = ply.__dsit_confirmed or {}
			ply.__dsit_confirmed[ply.__dsit_confirm_last] = RealTime() + 600
			ply.__dsit_confirm_last = nil
		else
			messaging.LChatPlayer2(ply, 'message.dsit.status.confirm_none')
		end

		return ''
	end
end

local function PlayerDisconnected(self)
	if IsValid(self:GetVehicle()) then
		DSit_PlayerLeaveVehicle(self, self:GetVehicle())
	end

	for _, ply in ipairs(player.GetAll()) do
		if ply.__dsit_confirmed then
			ply.__dsit_confirmed[self] = nil
		end

		if ply.__dsit_confirm_spam then
			ply.__dsit_confirm_spam[self] = nil
		end
	end
end

local function dsit_confirm_sit(ply)
	local ply2 = ply.__dsit_confirm_last
	if not IsValid(ply2) then return end
	ply.__dsit_confirm_last = nil

	ply.__dsit_confirmed = ply.__dsit_confirmed or {}
	ply.__dsit_confirmed[ply2] = RealTime() + 600
	messaging.LChatPlayer2(ply2, 'message.dsit.status.confirmed', ply)
end

hook.Add('PlayerLeaveVehicle', 'DSit', DSit_PlayerLeaveVehicle)
hook.Add('PlayerDeath', 'DSit', PlayerDeath)
hook.Add('PlayerSay', 'DSit', PlayerSay)
hook.Add('PlayerPostThink', 'DSit', PlayerPostThink)
hook.Add('PlayerDisconnected', 'DSit', PlayerDisconnected)
concommand.Add('dsit_confirm', dsit_confirm_sit)
